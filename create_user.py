from flask import Flask, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from app.models import User
from getpass import getpass
from app import create_app
import sys
import os

app = create_app()
db = SQLAlchemy()
db.init_app(app)
bcrypt = Bcrypt()

def main():
    with app.app_context():
        db.metadata.create_all(db.engine)
        print('Enter email address: ')
        email = input()
        password = getpass()
        assert password == getpass('Password (again):')

        user = User(
            email=email, 
            password=bcrypt.generate_password_hash(password))
        db.session.add(user)
        db.session.commit()
        print('User added.')


if __name__ == '__main__':
    sys.exit(main())