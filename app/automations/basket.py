from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from sqlalchemy.exc import IntegrityError
from rq import Queue
from rq import get_current_job
from rq.job import Job
from worker import conn
from app import db
from app.models import Product,Variant,Retailer,Offer,Lightbox,BasketAutomationJob
import os

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
db.init_app(app)

def get_product(username,password,products):
    with app.app_context():
                
        result = get_product_data(products)

        try:
            db.session.add(BasketAutomationJob(
                login_success = False,
                basket_success = False,
                job_started = datetime.now(),
                job_finished = datetime.now(),
                job_id = result["job_id"]
            ))
            db.session.commit()
        except exc.IntegrityError as e:
            print(e)
            print("Unable to add job item to database.")

def get_product_data(products):
    with app.app_context():
        job = get_current_job()
        start_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        
        driver = start_chrome()
        filename = "{}_product_page.png".format(job.id)
        errors = []
        
        for product in products:   
            try:
                driver.get(product["url"])
                driver.save_screenshot("./screenshots/" + filename)
                
                name = driver.find_element_by_xpath("//h1[@itemprop='name']").get_attribute("content")
                sku = driver.find_element_by_xpath("//meta[@itemprop='sku']").get_attribute("content")
                brand = driver.find_element_by_xpath("//span[@itemprop='brand']").text
                category = driver.find_element_by_xpath("//a[@itemprop='item']").text
                mpn = driver.find_element_by_xpath("//meta[@itemprop='model']").get_attribute("content")
                price_now = driver.find_element_by_xpath("//span[@itemprop='price']").get_attribute("content")
                currency = driver.find_element_by_xpath("//span[@itemprop='priceCurrency']").get_attribute("content")
                
                try:
                    db.session.add(Product(
                        name = name,
                        sku = sku,
                        mpn = mpn,
                        price_now = price_now,
                        currency = currency,
                        brand = brand,
                        category = category,
                        job_id = job.id,
                        url = driver.current_url
                    ))
                    db.session.commit()
                except exc.IntegrityError as e:
                    errors.append("Unable to add product to database.")
                    print(errors)

                driver.close()

            except:
                errors.append("Couldn't locate product elements")

        end_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        result = {
            "job_started": start_time,
            "job_finished": start_time,
            "job_id": job.id
        }

        return result

# Step 1 - Retrieve the correct offer url from the database
def get_offers(offers):
    with app.app_context():
        product_offers = []

        for offer in offers:
            try:
                db_offer = Offer.query.get(offer["id"])
                products = {
                    "id": db_offer.id,
                    "url": db_offer.url,
                    "quantity": offer["quantity"]
                }
                product_offers.append(products)
            except exc.IntegrityError as e:
                print(e)
        return product_offers

# Step 2 - Prepare automation environment
def add_to_basket(username, password, offers, retailer_id):
    with app.app_context():
        product_offers = get_offers(offers)
        retailer = Retailer.query.get(retailer_id)
        
        automations = {
            'tesco_uk': tesco_add_to_basket(username, password, product_offers)
        }
        
        result = automations.get(retailer.automation_class)

        try:
            db.session.add(BasketAutomationJob(
                login_success = result["login_success"],
                basket_success = result["basket_success"],
                job_started = result["job_started"],
                job_finished = result["job_finished"],
                job_id = result["job_id"]
            ))
            db.session.commit()
            print("Item added to the database")
        except exc.IntegrityError as e:
            print(e)
            print("Unable to add job item to database.")

def start_chrome():
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    options.add_argument("--window-size=1920,1080")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-gpu")
    #driver = webdriver.Remote(command_executor='http://localhost:4444/wd/hub', desired_capabilities=DesiredCapabilities.CHROME)
    driver = webdriver.Chrome(os.environ['CHROMEDRIVER_PATH'], chrome_options=options)
    return driver

# Retailer specific functions
def tesco_add_to_basket(username, password, product_offers):
    with app.app_context():
        start_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        driver = start_chrome()
        job = get_current_job()
        errors = []
        
        try:
            driver.get('https://secure.tesco.com/account/en-GB/login')
            login_username = driver.find_element_by_id('username')
            login_username.send_keys(username)
            login_password = driver.find_element_by_id('password')
            login_password.send_keys(password)
            login_password.submit()
            # TODO: Check for successful login
            job.meta['login_success'] = True
            job.save_meta()
        except:
            job.meta['login_success'] = False
            job.save_meta()
    
        for offer in product_offers:
                try:
                    print('Starting Add to Basket run')
                    driver.get(offer["url"])
                    print("Adding to basket: " + str(offer["url"]))
                    add_to_basket = driver.find_element_by_class_name('add-control')
                    add_to_basket.click()
                    # TODO: Check for successful add to cart
                    driver.save_screenshot("/home/ec2-user/environment/python_phantomjs/screenshots/add_to_cart.png")
                except:
                    driver.save_screenshot("/home/ec2-user/environment/python_phantomjs/screenshots/add_to_basket_error.png")
                    errors.append(offer["id"])
                    print("Error adding offer: " + str(offer["id"]))
                    # TODO: Record specifically which offers could not be added
                    
        if len(errors) > 0:
            job.meta['basket_success'] = False
        else:
            job.meta['basket_success'] = True
            
        end_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        job.save_meta()
        driver.quit()
        
        result = {
            "login_success": job.meta['login_success'],
            "basket_success": job.meta['basket_success'],
            "job_started": start_time,
            "job_finished": end_time,
            "job_id": job.id
        }
        
        return result