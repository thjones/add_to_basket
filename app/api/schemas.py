from flask import Flask
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields, ValidationError, pre_load

app = Flask(__name__)
ma = Marshmallow(app)

class LightboxSchema(ma.Schema):
    id = fields.Int()
    name = fields.String()

class RetailerSchema(ma.Schema):
    id = fields.Int()
    name = fields.String()

class OfferSchema(ma.Schema):
    id = fields.Int()
    retailer_id = fields.Int()
    in_stock = fields.Boolean()
    price_now = fields.Float()
    price_was = fields.Float()

class VariantSchema(ma.Schema):
    id = fields.Int()
    name = fields.String()
    sku = fields.String()
    offers = fields.Nested(OfferSchema(), many=True)

class ProductSchema(ma.Schema):
    id = fields.Int()
    name = fields.String()
    sku = fields.String()
    mpn = fields.String()
    brand = fields.String()
    category = fields.String()
    price_now = fields.Float()
    currency = fields.String()

class BasketAutomationJobSchema(ma.Schema):
    job_id = fields.String()

lightbox_schema = LightboxSchema()
offers_schema = OfferSchema(many=True)
products_schema = ProductSchema(many=True)
retailers_schema = RetailerSchema(many=True)
job_schema = BasketAutomationJobSchema()