from datetime import datetime
from flask import Flask, request, jsonify
from rq import Queue
from rq import get_current_job
from rq.job import Job
from worker import conn
from flask_marshmallow import Marshmallow
from flask import Blueprint
import os
import json
from app.models import Product,Variant,Retailer,Offer,Lightbox,BasketAutomationJob
from app import db
from app.api.schemas import lightbox_schema, offers_schema, products_schema, retailers_schema, job_schema
from app.automations.basket import add_to_basket, get_product
from . import api_blueprint

q = Queue(connection=conn)

@api_blueprint.route('/basket', methods=["POST"])
def initiate_basket_run():
    data = json.loads(request.data.decode())
    offers = data["offers"]
    username = data["username"]
    password = data["password"]
    retailer_id = data["retailer_id"]
    
    job = q.enqueue_call(
        func=add_to_basket, args=(username,password,offers,retailer_id), result_ttl=5000
    )
    return job.get_id()

@api_blueprint.route('/product', methods=["POST"])
def intiate_product_run():
    data = json.loads(request.data.decode())
    username = data["username"]
    password = data["password"]
    products = data["products"]
    
    job = q.enqueue_call(
        func=get_product, args=(username,password,products), result_ttl=5000
    )
    return job.get_id()

@api_blueprint.route('/request/<job_key>', methods=["GET"])
def get_request_results(job_key):
    
    job = Job.fetch(job_key, connection=conn)
    products_result = products_schema.dump(Product.query.filter_by(job_id = job.id))

    if job.is_finished:
        return jsonify({'request': job.id, 'products': products_result.data})
        return jsonify(result), 200
    else:
        result = { "request": job.id, "status": "processing" }
        return jsonify(result), 202

@api_blueprint.route("/status/<job_key>", methods=['GET'])
def get_job_status(job_key):

    job = Job.fetch(job_key, connection=conn)

    if job.is_finished:
        result = { "request": job.id, "status": "finished" }
        return jsonify(result), 200
    else:
        result = { "request": job.id, "status": "processing" }
        return jsonify(result), 202

@api_blueprint.route('/lightbox/<lightbox_id>', methods=["GET"])
def get_lightbox(lightbox_id):
    lightbox = Lightbox.query.filter_by(id = lightbox_id).first()
    
    lightbox_result = lightbox_schema.dump(lightbox)
    products_result = products_schema.dump(lightbox.lightbox_products.query.all())
    
    return jsonify({'lightbox': lightbox_result.data, 'products': products_result.data})