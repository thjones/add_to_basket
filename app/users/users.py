from flask import (Flask, Blueprint, send_from_directory, abort, request,
                   render_template, current_app, render_template, redirect,
                   url_for, current_app)
from flask_login import LoginManager, login_required, login_user, logout_user, current_user
from flask_bcrypt import Bcrypt
from app import db
from app.models import User
from app.forms import LoginForm
from app.models import Product,Variant,Retailer,Offer,Lightbox,BasketAutomationJob
from . import users_blueprint

app = Flask(__name__)
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)

@users_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.get(form.email.data)
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user, remember=True)
    return render_template("login.html", form=form)

@users_blueprint.route("/logout")
@login_required
def logout():
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return render_template("logout.html")