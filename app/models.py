from app import db
from datetime import datetime

class BasketAutomationJob(db.Model):
    __tablename__ = 'basket_automation_jobs'

    id = db.Column(db.Integer, primary_key=True)
    job_started = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    job_finished = db.Column(db.DateTime, index=True)
    login_success = db.Column(db.Boolean, default=False)
    basket_success = db.Column(db.Boolean, default=False)
    job_id = db.Column(db.String, index=True)

    def __init__(self, job_started, job_finished, login_success, basket_success, job_id):
        self.job_started = job_started
        self.job_finished = job_finished
        self.login_success = login_success
        self.basket_success = basket_success
        self.job_id = job_id
        
    def __repr__(self):
        return '<id {}>'.format(self.id)
        
class Product(db.Model):
    __tablename__ = 'products'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True)
    sku = db.Column(db.String)
    mpn = db.Column(db.String)
    brand = db.Column(db.String)
    category = db.Column(db.String)
    reviews = db.Column(db.Boolean, default=False)
    in_stock = db.Column(db.Boolean, index=True, default=False)
    price_now = db.Column(db.Float)
    currency = db.Column(db.String)
    job_id = db.Column(db.String, index=True)
    url = db.Column(db.String, index=True)
    variants = db.relationship('Variant', backref='product', uselist=False, lazy=True)

    def __repr__(self):
        return '<id {}>'.format(self.id)
        
class Variant(db.Model):
    __tablename__ = 'product_variants'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True)
    sku = db.Column(db.String, index=True)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'), nullable=False)
    offers = db.relationship('Offer', backref='variant', lazy=True)

    def __repr__(self):
        return '<id {}>'.format(self.id)
        
class Retailer(db.Model):
    __tablename__ = 'retailers'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True)
    offers = db.relationship('Offer', backref='retailer', lazy=True)
    automation_class = db.Column(db.String, index=True)

    def __repr__(self):
        return '<id {}>'.format(self.id)
        
class Offer(db.Model):
    __tablename__ = 'offers'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String, index=True)
    price_now = db.Column(db.Float)
    price_was = db.Column(db.Float)
    variant_id = db.Column(db.Integer, db.ForeignKey('product_variants.id'), nullable=False)
    retailer_id = db.Column(db.Integer, db.ForeignKey('retailers.id'))

    def __repr__(self):
        return '<id {}>'.format(self.id)
 
lightbox_products = db.Table('lightbox_products',
    db.Column('lightbox_id', db.Integer, db.ForeignKey('lightboxes.id'), nullable=False),
    db.Column('product_id', db.Integer, db.ForeignKey('products.id'), nullable=False),
    db.PrimaryKeyConstraint('lightbox_id', 'product_id')
)

class Lightbox(db.Model):
    __tablename__ = 'lightboxes'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True)
    lightbox_products = db.relationship('Product', secondary=lightbox_products, backref='lightbox', uselist=False, lazy=True)

    def __repr__(self):
        return '<id {}>'.format(self.id)

class User(db.Model):
    __tablename__ = 'user'

    email = db.Column(db.String, primary_key=True)
    password = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.email

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False